<%-- 
    Document   : index
    Created on : Mar 24, 2012, 11:41:33 AM
    Author     : 4R135
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistem Pembelian Buku</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <%@include  file="view/Header.jsp" %>
        <div id="menu">
        <table>            
            <tr>
                <td>
                    <b>Master Data</b><br>
                    <a href="FormPembeli">Pembeli</a><br>
                    <a href="FormBuku">Buku</a><br>
                    <b>Mencetak</b><br>
                    <a href="MencetakKwitansi">Kwitansi</a>
                </td>
                <td>
                    <h1>Selamat Datang</h1>
                    <p>Di Toko Buku Online</p>
                    <div id="author">
                    <p>Disusun Oleh:</p>
                    <ol>
                        <li>Imron Rosdiana</li>
                        <li>Milkis</li>
                        <li>Arif Patra Jumena</li>
                    </ol>
                    </div>
                </td>
            </tr>
        </table>
        </div>
        <jsp:include page="view/Footer.jsp" />
    </body>
</html>
