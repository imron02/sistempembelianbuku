<%-- 
    Document   : Footer
    Created on : Mar 24, 2012, 1:51:00 PM
    Author     : 4R135
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistem Pembelian Buku</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="footer">
            <p>Copyright &copy; 2013 Toko Online<br>
            Kp. Nagreg No. 5 Tangerang - Banten</p>
        </div>            
    </body>
</html>
