-- phpMyAdmin SQL Dump
-- version 3.5.7deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2013 at 10:26 PM
-- Server version: 5.5.29-0ubuntu1
-- PHP Version: 5.4.9-4ubuntu2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbaplikasipembelianbuku`
--
CREATE DATABASE `dbaplikasipembelianbuku` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbaplikasipembelianbuku`;

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE IF NOT EXISTS `buku` (
  `kode_buku` int(11) NOT NULL,
  `judul_buku` varchar(75) DEFAULT NULL,
  `penerbit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode_buku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`kode_buku`, `judul_buku`, `penerbit`) VALUES
(123, 'Pemograman Java', 'Informatika'),
(124, 'Mysql', ' Andi'),
(125, 'Pemograman Web', 'Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `kwitansi`
--

CREATE TABLE IF NOT EXISTS `kwitansi` (
  `no_beli` int(11) NOT NULL,
  `kode_buku` int(11) NOT NULL,
  `Jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  PRIMARY KEY (`no_beli`,`kode_buku`),
  KEY `kode_buku` (`kode_buku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kwitansi`
--

INSERT INTO `kwitansi` (`no_beli`, `kode_buku`, `Jumlah`, `harga`) VALUES
(1, 123, 1, 65000),
(1, 124, 4, 40000),
(2, 123, 2, 50000),
(2, 125, 1, 60000),
(3, 124, 1, 50000);

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE IF NOT EXISTS `pembeli` (
  `no_beli` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `tlp` varchar(13) DEFAULT NULL,
  PRIMARY KEY (`no_beli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`no_beli`, `nama`, `alamat`, `tlp`) VALUES
(1, 'Imron', 'Kp. Nagreg', '08979127446'),
(2, 'Arif Patra', 'Tangerang', '08979800555'),
(3, 'Milkis', 'Radal', '0812830112');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kwitansi`
--
ALTER TABLE `kwitansi`
  ADD CONSTRAINT `kwitansi_ibfk_1` FOREIGN KEY (`no_beli`) REFERENCES `pembeli` (`no_beli`),
  ADD CONSTRAINT `kwitansi_ibfk_2` FOREIGN KEY (`kode_buku`) REFERENCES `buku` (`kode_buku`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
