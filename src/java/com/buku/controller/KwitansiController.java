/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buku.controller;

import com.buku.model.Koneksi;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author aim
 */
@WebServlet(name = "KwitansiController", urlPatterns = {"/KwitansiController"})
public class KwitansiController extends HttpServlet {
    
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }
    
    public boolean cetakLaporan(String opsi, String no_beli, String nama, HttpServletResponse response){
       boolean adaKesalahan = false;	
        Connection cn = null; 
        
       try{ 
	  Class.forName(Koneksi.driver); 
	} catch (Exception ex){ 
	  adaKesalahan = true; 
	  errorMessage = "JDBC Driver tidak ditemukan atau rusak\n"+ex;  
	} 
	
	if (!adaKesalahan){ 
	  try { 
	    cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+""); 
	  } catch (Exception ex) { 
	    adaKesalahan = true;  
		errorMessage = "Koneksi ke "+Koneksi.database+" gagal\n"+ex; 
	  } 
	  
	  if (!adaKesalahan){ 
              String SQLStatement;
              try {
                  Statement statement = cn.createStatement();
                  
                  SQLStatement = "SELECT buku.`kode_buku` AS buku_kode_buku, "
                          +"buku.`judul_buku` AS buku_judul_buku, "
                          +"buku.`penerbit` AS buku_penerbit, "
                          +"kwitansi.`no_beli` AS kwitansi_no_beli, "
                          +"kwitansi.`kode_buku` AS kwitansi_kode_buku, "
                          +"kwitansi.`Jumlah` AS kwitansi_Jumlah,"
                          + "kwitansi.`harga` AS kwitansi_harga, "
                          +"(kwitansi.`harga`*kwitansi.`jumlah`) AS kwitansi_totalharga, "
                          +"pembeli.`no_beli` AS pembeli_no_beli, "
                          +"pembeli.`nama` AS pembeli_nama, "
                          +"pembeli.`alamat` AS pembeli_alamat, "
                          +"pembeli.`tlp` AS pembeli_tlp "
                          +"FROM "
                          +"`buku` buku INNER JOIN `kwitansi` kwitansi ON buku.`kode_buku` = kwitansi.`kode_buku` "
                          +"INNER JOIN `pembeli` pembeli ON kwitansi.`no_beli` = pembeli.`no_beli` ";                          
                  
                  if (!opsi.equals("Semua")){
                      if (opsi.equals("No_beli")){
                          SQLStatement = SQLStatement + " where pembeli.`no_beli`="+no_beli;
                      }
                      else {
                          SQLStatement = SQLStatement + " where pembeli.`nama`='"+nama+"'";                                  
                      }
                  }
                  
                  SQLStatement = SQLStatement +" ORDER BY "
                          +"pembeli.`tlp` ASC, "
                          +"pembeli.`nama` ASC";
                  
                  JasperDesign disain = JRXmlLoader.load(getServletConfig().getServletContext().getRealPath("reports/KwitansiReport.jrxml"));
		  JasperReport nilaiLaporan = JasperCompileManager.compileReport(disain);
                  
                  ResultSet resultSet = statement.executeQuery(SQLStatement);
                  JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(resultSet);
                  
                  JasperPrint cetak = JasperFillManager.fillReport(nilaiLaporan,new HashMap(),resultSetDataSource);
		  
		  byte[] pdfasbytes = JasperExportManager.exportReportToPdf(cetak);
                                   
                  OutputStream outStream = response.getOutputStream();
                  response.setHeader("Content-Disposition","inline; filename=KwitansiReport.pdf");
                  response.setContentType("application/pdf");
                  response.setContentLength(pdfasbytes.length);
                  outStream.write(pdfasbytes,0,pdfasbytes.length);
            
                  outStream.flush();
                  outStream.close();                  
	    } catch (Exception ex) {  
                errorMessage = "Gagal mencetak\n"+ex;
            } 
          }
        }
        
        return !adaKesalahan;
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        RequestDispatcher rd;        
        
        String opsi = request.getParameter("opsi");
        String no_beli = request.getParameter("no_beli");
        String nama = request.getParameter("nama");
                
        if (no_beli==null) no_beli="";
        if (opsi==null) opsi="Semua";
        if (nama==null) nama="";
        
        String keterangan="<br>";
        
        rd = request.getRequestDispatcher("Header.jsp"); 
        rd.include(request, response);

        out.println("<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />");
        out.println("<div id='kwitansi'>");
        out.println("<center>");
        out.println("<table width='80%' bgcolor='#eeeeee'>");
        out.println("<tr height='400'>");
        out.println("<td width=200 align=center valign=top bgcolor='#eeffee'>");
        out.println("<a href='index.jsp'>Home</a><br>");
        out.println("<b>Master Data</b><br>");
        out.println("<a href='FormPembeli'>Pembeli</a><br>");
        out.println("<a href='Buku.jsp'>Buku</a><br>");
        out.println("<b>Mencetak</b><br>");
        out.println("<a href='MencetakKwitansi'>Kwitansi</a>"); 
        out.println("</td>");
        
        out.println("<td height=400 valign=top bgcolor='#FFFFFF'>");
        out.println("<center>");
        out.println("<h2>Mencetak Kwitansi</h2>");
        out.println("<form action='MencetakKwitansi' method='post'>");
        out.println("<table>");        
        
        try{
            out.println("<tr>");
            if (opsi.equals("No_beli")){
                out.println("<td align='right'><input type='radio' checked name='opsi' value='No_beli'></td>");
            } else {
                out.println("<td align='right'><input type='radio' name='opsi' value='No_beli'></td>");
            }            
            out.println("<td align='left'>No Beli</td>");
            out.println("<td align='left'><input type='text' value='"+no_beli+"' name='no_beli' maxlength='15' size='15'></td>");
            out.println("</tr>");
            
            out.println("<tr>");
            if (opsi.equals("Namaku")){
                out.println("<td align='right'><input type='radio' checked name='opsi' value='Namaku'></td>");
            } else {
                out.println("<td align='right'><input type='radio' name='opsi' value='Namaku'></td>");
            }            
            out.println("<td align='left'>Nama</td>");
            out.println("<td align='left'><input type='text' value='"+nama+"' name='nama' maxlength='15' size='15'></td>");
            out.println("</tr>");
            
            out.println("<tr>");
            if (opsi.equals("Semua")){
                out.println("<td align='right'><input type='radio' checked name='opsi' value='Semua'></td>");
            } else {
                out.println("<td align='right'><input type='radio' name='opsi' value='Semua'></td>");
            }
            out.println("<td align='left'>Semua</td>");
            out.println("<td><br></td>");            
            out.println("</tr>");
                        
            out.println("<tr>");
            out.println("<td colspan='3'><b>"+keterangan+"</b></td>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<td colspan='3' align='center'><input type='submit' name='tombol' value='Cetak' style='width: 100px'></td>");
            out.println("</tr>");
            
            out.println("</table>");
            out.println("</form>");
            out.println("</center>");
            out.println("</td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("</center>");
            out.println("</div>");
            
            rd = request.getRequestDispatcher("Footer.jsp"); 
            rd.include(request, response);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String tombol = request.getParameter("tombol");
        String opsi = request.getParameter("opsi");
        String no_beli = request.getParameter("no_beli");
        String nama = request.getParameter("nama");
                
        if (tombol==null) tombol="";
        if (no_beli==null) no_beli="";
        if (opsi==null) opsi="";
        if (nama==null) nama="";
        
        if (tombol.equals("Cetak")){
            if (!cetakLaporan(opsi, no_beli, nama, response)){                
            } else {
                processRequest(request, response);
            }
        }else {
            processRequest(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
