/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buku.controller;

import com.buku.model.Buku;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aim
 */
@WebServlet(name = "BukuController", urlPatterns = {"/BukuController"})
public class BukuController extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        RequestDispatcher rd;
        Buku buku = new Buku();

        String tombol = request.getParameter("tombol");
        String kode_buku = request.getParameter("kode_buku");
        String judul_buku = request.getParameter("judul_buku");
        String penerbit = request.getParameter("penerbit");
        String mulaiParameter = request.getParameter("mulai");
        String jumlahParameter = request.getParameter("jumlah");
        String kodeBukuDipilih = request.getParameter("kodeBukuDipilih");

        if (tombol==null) tombol="";
        if (kode_buku==null) kode_buku="";
        if (judul_buku==null) judul_buku="";
        if (penerbit==null) penerbit="";
        if (kodeBukuDipilih==null) kodeBukuDipilih="";

        int mulai=0, jumlah=10;

        try{
            mulai = Integer.parseInt(mulaiParameter);
        } catch (Exception ex){}

        try{
            jumlah = Integer.parseInt(jumlahParameter);
        } catch (Exception ex){}

        String keterangan="<br>";

        rd = request.getRequestDispatcher("Header.jsp");
        rd.include(request, response);

        out.println("<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />");
        out.println("<div id='buku'>");
        out.println("<center>");
        out.println("<table width='80%' bgcolor='#eeeeee'>");
        out.println("<tr height='400'>");
        out.println("<td width=200 align=center valign=top bgcolor='#eeffee'>");
        out.println("<a href='index.jsp'>Home</a><br>");
        out.println("<b>Master Data</b><br>");
        out.println("<a href='FormPembeli'>Pembeli</a><br>");
        out.println("<a href='FormBuku'>Buku</a><br>");
        out.println("<b>Mencetak</b><br>");
        out.println("<a href='MencetakKwitansi'>Kwitansi</a>");
        out.println("</td>");

        out.println("<td height=400 valign=top bgcolor='#FFFFFF'>");
        out.println("<center>");
        out.println("<h2>Master Data Buku</h2>");
        out.println("<form action='FormBuku' method='post'>");
        out.println("<table>");

        try {
            if (tombol.equals("Simpan")){
                if (!kode_buku.equals("")){
                    buku.setKodeBuku(kode_buku);
                    buku.setJudulBuku(judul_buku);
                    buku.setPenerbit(penerbit);

                    if (buku.simpan()){
                        kode_buku="";
                        judul_buku="";
                        penerbit="";
                        keterangan="Sudah tersimpan";
                    } else {
                        keterangan="Gagal menyimpan:\n"+buku.getErrorMessage();
                    }
                } else {
                    keterangan="Gagal menyimpan, kode buku tidak boleh kosong";
                }
            }

            if (tombol.equals("Daftar")){

            }

            if (tombol.equals("Hapus")){
               if (!kode_buku.equals("")){
                    if (buku.hapus(kode_buku)){
                        kode_buku="";
                        judul_buku="";
                        penerbit="";
                        keterangan="Data sudah dihapus";
                    } else {
                        keterangan="Kode buku tersebut tidak ada, atau ada kesalahan:\n"+buku.getErrorMessage();
                    }
                } else {
                    keterangan="Kode buku masih kosong";
                }
            }

            if (tombol.equals("Cari")){
                if (!kode_buku.equals("")){
                    if (buku.cari(kode_buku)){
                        kode_buku=buku.getKodeBuku();
                        judul_buku=buku.getJudulBuku();
                        penerbit=buku.getPenerbit();
                        keterangan="<br>";
                    } else {
                        keterangan="Kode buku tersebut tidak ada";
                    }
                } else {
                    keterangan="Kode buku masih kosong";
                }
            }

            if (tombol.equals("Pilih")){
                kode_buku=kodeBukuDipilih;
                judul_buku="";
                penerbit="";
                if (!kodeBukuDipilih.equals("")){
                    if (buku.cari(kodeBukuDipilih)){
                        kode_buku=buku.getKodeBuku();
                        judul_buku=buku.getJudulBuku();
                        penerbit=buku.getPenerbit();
                        keterangan="<br>";
                    } else {
                        keterangan="Kode buku tersebut tidak ada";
                    }
                } else {
                    keterangan="Tidak ada yang dipilih";
                }
            }
        }catch (Exception ex){
            keterangan="Ada kesalahan: "+ex.getMessage();
        }

        try{
            out.println("<tr>");
            out.println("<td align='right'>Kode Buku</td>");
            out.println("<td align='left'><input type='text' value='"+kode_buku+"' name='kode_buku' maxlength='15' size='15'><input type='submit' name='tombol' value='Cari'></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td align='right'>Judul Buku</td>");
            out.println("<td align='left'><input type='text' value='"+judul_buku+"' name='judul_buku' maxlength='30' size='30'></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td align='right'>Penerbit</td>");
            out.println("<td align='left'><input type='text' value='"+penerbit+"' name='penerbit' maxlength='30' size='30'></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td colspan='2'><b>"+keterangan+"</b></td>");
            out.println("</tr>");

            out.println("<tr>");
            out.println("<td colspan='2' align='center'>");
            out.println("<table>");
            out.println("<tr>");
            out.println("<td align='center'><input type='submit' name='tombol' value='Simpan' style='width: 100px'></td>");
            out.println("<td align='center'><input type='submit' name='tombol' value='Hapus' style='width: 100px'></td>");
            out.println("<td align='center'><input type='submit' name='tombol' value='Daftar' style='width: 100px'></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("</td>");
            out.println("</tr>");

            out.println("<tr>");
            out.println("<td colspan='2' align='center'><br>");
            out.println("</td>");
            out.println("</tr>");

            if (tombol.equals("Daftar") || tombol.equals("Sebelumnya") || tombol.equals("Berikutnya") || tombol.equals("Tampilkan")){
                out.println("<tr>");
                out.println("<td colspan='2' align='center'>");
                out.println("<div class='data'>");
                out.println("<table>");

                if (tombol.equals("Sebelumnya")){
                    mulai -= jumlah;
                    if (mulai < 0 ) mulai=0;
                }

                if (tombol.equals("Berikutnya")){
                    mulai += jumlah;
                }

                Object[][] listBuku = buku.bacaDaftar(mulai,jumlah);
                for (int i=0; i<listBuku.length; i++){
                    out.println("<tr>");
                    out.println("<td>");
                    if (i == 0){
                        out.println("<input type='radio' checked name='kodeBukuDipilih' value='"+listBuku[i][0].toString()+"'>");
                    } else {
                        out.println("<input type='radio' name='kodeBukuDipilih' value='"+listBuku[i][0].toString()+"'>");
                    }
                    out.println("</td>");
                    out.println("<td>");
                    out.println(listBuku[i][0].toString());
                    out.println("</td>");
                    out.println("<td>");
                    out.println(listBuku[i][1].toString());
                    out.println("</td>");
                    out.println("</tr>");
                }

                out.println("</table>");
                out.println("</div>");
                out.println("</td>");
                out.println("</tr>");

                out.println("<tr>");
                out.println("<td colspan='2' align='center'>");
                out.println("<table>");
                out.println("<tr>");
                out.println("<td align='center'><input type='submit' name='tombol' value='Sebelumnya' style='width: 100px'></td>");
                out.println("<td align='center'><input type='submit' name='tombol' value='Pilih' style='width: 60px'></td>");
                out.println("<td align='center'><input type='submit' name='tombol' value='Berikutnya' style='width: 100px'></td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td align='center'>Mulai <input type='text' name='mulai' value="+mulai+" style='width: 40px'></td>");
                out.println("<td>Jumlah");
                out.println("<select name='jumlah'>");
                for (int i=1; i<=10; i++) {
                    if (jumlah == (i*10)){
                        out.println("<option selected value="+i*10+">"+i*10+"</option>");
                    } else {
                        out.println("<option value="+i*10+">"+i*10+"</option>");
                    }
                }
                out.println("</select>");
                out.println("</td>");
                out.println("<td align='center'><input type='submit' name='tombol' value='Tampilkan' style='width: 90px'></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("</td>");
                out.println("</tr>");
            }

            out.println("</table>");
            out.println("</form>");
            out.println("</center>");
            out.println("</td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("</center>");
            out.println("</div>");
            rd = request.getRequestDispatcher("Footer.jsp");
            rd.include(request, response);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
