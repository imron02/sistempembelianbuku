/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buku.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author aim
 */
public class Buku {
    private String kodeBuku, judulBuku, Penerbit, errorMessage;

    public String getKodeBuku() {
        return kodeBuku;
    }

    public void setKodeBuku(String kodeBuku) {
        this.kodeBuku = kodeBuku;
    }

    public String getJudulBuku() {
        return judulBuku;
    }

    public void setJudulBuku(String judulBuku) {
        this.judulBuku = judulBuku;
    }

    public String getPenerbit() {
        return Penerbit;
    }

    public void setPenerbit(String Penerbit) {
        this.Penerbit = Penerbit;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
   public boolean simpan(){
        boolean adaKesalahan = false;
	Connection cn = null;

	try{
	  Class.forName(Koneksi.driver);
	} catch (Exception ex){
	  adaKesalahan = true;
	  errorMessage = "JDBC Driver tidak ditemukan atau rusak\n"+ex;
	}

	if (!adaKesalahan){
	  try {
	    cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
	  } catch (Exception ex) {
	    adaKesalahan = true;
		errorMessage = "Koneksi ke "+Koneksi.database+" gagal\n"+ex;
	  }

	  if (!adaKesalahan){
              int jumlahSimpan=0;
		boolean simpan = false;

		try {
		  String SQLStatemen = "select * from buku where kode_buku='"+kodeBuku+"'";
		  Statement sta = cn.createStatement();
		  ResultSet rset = sta.executeQuery(SQLStatemen);

		  rset.next();
		  if (rset.getRow()>0){
                      sta.close();
                      rset.close();

                      simpan = true;
                      SQLStatemen = "update buku set judul_buku='"+judulBuku+"', penerbit='"+Penerbit+"' where kode_buku='"+kodeBuku+"'"; 
                      sta = cn.createStatement();
                      jumlahSimpan = sta.executeUpdate(SQLStatemen);
		  } else {
		    sta.close();
			rset.close();

			simpan = true;
			SQLStatemen = "insert into buku(kode_buku, judul_buku, penerbit) values ('"+ kodeBuku +"','"+ judulBuku +"','"+ Penerbit +"')"; 
                        sta = cn.createStatement();
			jumlahSimpan = sta.executeUpdate(SQLStatemen);
		  }

		  if (simpan) {
		    if (jumlahSimpan > 0){
			  errorMessage = "Data buku sudah tersimpan";
			} else {
			  adaKesalahan = true;
			  errorMessage = "Gagal menyimpan data buku";
			}
		  }
		} catch (Exception ex){
		  adaKesalahan = true;
		  errorMessage = "Tidak dapat membuka tabel buku\n"+ex;
		}
          }
        }
        return !adaKesalahan;
  }

  public boolean cari(String kode_buku){
      boolean adaKesalahan = false;
      Connection cn = null;

      try{
          Class.forName(Koneksi.driver);
      } catch (Exception ex){
          adaKesalahan = true;
          errorMessage = "JDBC Driver tidak ditemukan atau rusak\n"+ex;
      }

      if (!adaKesalahan){
          try {
	    cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
	  } catch (Exception ex) {
	    adaKesalahan = true;
		errorMessage = "Koneksi ke "+Koneksi.database+" gagal\n"+ex;
	  }

	  if (!adaKesalahan){
              try {
                  String SQLStatemen = "select * from buku where kode_buku='"+kode_buku+"'";
                  Statement sta = cn.createStatement();
		  ResultSet rset = sta.executeQuery(SQLStatemen);

		  rset.next();
		  if (rset.getRow()>0){
                      this.kodeBuku = rset.getString("kode_buku");
                      this.judulBuku = rset.getString("judul_buku");
                      this.Penerbit = rset.getString("penerbit");
                      sta.close();
                      rset.close();
                  } else {
		    sta.close();
			rset.close();
			adaKesalahan = true;
			errorMessage = "Kode buku \""+kode_buku+"\" tidak ditemukan";
		  }
		} catch (Exception ex){
		  adaKesalahan = true;
		  errorMessage = "Tidak dapat membuka tabel buku\n"+ex;
		}
	  }
	}
	return !adaKesalahan;
  }

  public Object[][] bacaDaftar(int mulai, int jumlah){
      boolean adaKesalahan = false;
	Connection cn = null;

	Object[][] daftarBuku = new Object[0][0] ;

	try{
	  Class.forName(Koneksi.driver);
	} catch (Exception ex){
	  adaKesalahan = true;
	  errorMessage = "JDBC Driver tidak ditemukan atau rusak\n"+ex;
	}

	if (!adaKesalahan){
            try {
                cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
            } catch (Exception ex) {
                adaKesalahan = true;
                errorMessage = "Koneksi ke "+Koneksi.database+" gagal\n"+ex;
            }

            if (!adaKesalahan){
                String SQLStatemen;
                Statement sta;
                ResultSet rset;

                try {
                    SQLStatemen = "select kode_buku,judul_buku from buku order by judul_buku asc, kode_buku asc limit "+mulai+","+jumlah;
                    sta = cn.createStatement();
                    rset = sta.executeQuery(SQLStatemen);

                    rset.next();
                    rset.last();
                    daftarBuku = new Object[rset.getRow()][2];
                    rset.first();
                    int i=0;
                    do {
                        daftarBuku[i] = new Object[]{rset.getString("kode_buku"), rset.getString("judul_buku")};
                        i++;
                    } while (rset.next());
                    sta.close();
                    rset.close();
                } catch (Exception ex){
                    errorMessage = "Tidak dapat membuka tabel buku\n"+ex;
                }
            }
        }

        return daftarBuku;
  }

  public boolean hapus(String kode_buku){
    boolean adaKesalahan = false;
	Connection cn = null;

	try{
	  Class.forName(Koneksi.driver);
	} catch (Exception ex){
	  adaKesalahan = true;
	  errorMessage = "JDBC Driver tidak ditemukan atau rusak\n"+ex;
	}

	if (!adaKesalahan){
	  try {
	    cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
	  } catch (Exception ex) {
	    adaKesalahan = true;
		errorMessage = "Koneksi ke database "+Koneksi.database+" gagal\n"+ex;
	  }

	  if (!adaKesalahan){
              int jumlahHapus;

              try {
                  String SQLStatemen = "delete from buku where kode_buku='"+kode_buku+"'";
		  Statement sta = cn.createStatement();
		  jumlahHapus = sta.executeUpdate(SQLStatemen);

		  if (jumlahHapus>0){
		    sta.close();
			errorMessage = "Data buku sudah dihapus";
		  } else {
		    sta.close();
			errorMessage = "Kode buku tidak ditemukan";
			adaKesalahan = true;
		  }

              } catch (Exception ex){
                  adaKesalahan = true;
		  errorMessage = "Tidak dapat membuka tabel buku\n"+ex;
              }
          }
	}
	return !adaKesalahan;
  }
}
