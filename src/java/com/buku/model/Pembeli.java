
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buku.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author aim
 */
public class Pembeli {
    private String no_beli,nama,alamat,tlp;
    public String errorMessage;

    public String getNo_beli() {
        return no_beli;
    }

    public void setNo_beli(String no_beli) {
        this.no_beli = no_beli;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTlp() {
        return tlp;
    }

    public void setTlp(String tlp) {
        this.tlp = tlp;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

   public boolean simpan(){
        boolean adaKesalahan = false;
        Connection cn = null;

	try{
            Class.forName(Koneksi.driver);
        } catch (Exception ex){
            adaKesalahan = true;
            errorMessage="JDBC Driver tidak ditemukan atau rusak\n"+ex;
	}

	if (!adaKesalahan){
            try {
                cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
            } catch (Exception ex) {
                adaKesalahan = true;
                errorMessage="Koneksi ke "+Koneksi.database+" gagal\n"+ex;
            }

            if (!adaKesalahan){
                int jumlahSimpan;
                boolean simpan;

                try {
                    String SQLStatemen = "select * from pembeli where no_beli='"+no_beli+"'"; 
                    Statement sta = cn.createStatement();
                    ResultSet rset = sta.executeQuery(SQLStatemen);

                    rset.next();
                    if (rset.getRow()>0){
                        sta.close();
                        rset.close();
                        simpan = true;
                        SQLStatemen = "update pembeli set nama='"+nama+"', alamat='"+alamat+"', tlp='"+tlp+ "' where no_beli='"+no_beli+"'"; 
                        sta = cn.createStatement();
			jumlahSimpan = sta.executeUpdate(SQLStatemen);
                    } else {
                        sta.close();
                        rset.close();

			simpan = true;
			SQLStatemen = "insert into pembeli(no_beli, nama, alamat, tlp) values ('"+ no_beli +"','"+ nama +"','"+ alamat +"','"+ tlp +"')"; 
                        sta = cn.createStatement();
			jumlahSimpan = sta.executeUpdate(SQLStatemen);
                    }

                    if (simpan) {
                        if (jumlahSimpan > 0){
                            errorMessage="Data pembeli sudah tersimpan";
                        } else {
                            adaKesalahan = true;
                            errorMessage="Gagal menyimpan data pembeli";
                        }
                    }
                } catch (Exception ex){
                    adaKesalahan = true;
                    errorMessage="Tidak dapat membuka tabel pembeli\n"+ex;
                }
            }
        }
        return !adaKesalahan;
    }

    public boolean cari(String no_beli){
        boolean adaKesalahan = false;
        Connection cn = null;

        try{
            Class.forName(Koneksi.driver);
        } catch (Exception ex){
            adaKesalahan = true;
            errorMessage="JDBC Driver tidak ditemukan atau rusak\n"+ex;
        }

        if (!adaKesalahan){
            try {
                cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
            } catch (Exception ex) {
                adaKesalahan = true;
                errorMessage="Koneksi ke "+Koneksi.database+" gagal\n"+ex;
            }

            if (!adaKesalahan){
                try {
                    String SQLStatemen = "select * from pembeli where no_beli='"+no_beli+"'"; 
                    Statement sta = cn.createStatement();
                    ResultSet rset = sta.executeQuery(SQLStatemen);

                    rset.next();
                    if (rset.getRow()>0){
                        this.no_beli = rset.getString("no_beli");
                        this.nama = rset.getString("nama");
                        this.alamat = rset.getString("alamat");
                        this.tlp = rset.getString("tlp");
                        sta.close();
                        rset.close();
                    } else {
                        sta.close();
                        rset.close();
                        adaKesalahan = true;
                        errorMessage="No Beli \""+no_beli+"\" tidak ditemukan";
                    }
                } catch (Exception ex){
                    adaKesalahan = true;
                    errorMessage="Tidak dapat membuka tabel pembeli\n"+ex;
                }
            }
        }
        return !adaKesalahan;
    }

    public Object[][] bacaDaftar(int mulai, int jumlah){
        boolean adaKesalahan = false;
        Connection cn = null;

        Object[][] daftarPembeli = new Object[0][0];

	try{
            Class.forName(Koneksi.driver);
	} catch (Exception ex){
            adaKesalahan = true;
            errorMessage="JDBC Driver tidak ditemukan atau rusak\n"+ex;
	}

	if (!adaKesalahan){
            try {
                cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
            } catch (Exception ex) {
                adaKesalahan = true;
                errorMessage="Koneksi ke "+Koneksi.database+" gagal\n"+ex;
            }

            if (!adaKesalahan){
                String SQLStatemen;
                Statement sta;
                ResultSet rset;

                try {
                    SQLStatemen = "select no_beli, nama from pembeli order by nama asc, no_beli asc limit "+mulai+","+jumlah;
                    sta = cn.createStatement();
                    rset = sta.executeQuery(SQLStatemen);

                    rset.next();
                    rset.last();
                    daftarPembeli = new Object[rset.getRow()][2];
                    rset.first();
                    int i=0;
                    do {
                        daftarPembeli[i] = new Object[]{rset.getString("no_beli"), rset.getString("nama")};
                        i++;
                    } while (rset.next());
                    sta.close();
                    rset.close();
                } catch (Exception ex){
                    errorMessage="Tidak dapat membuka tabel pembeli\n"+ex;
                }
            }
        }

        return daftarPembeli;
    }

    public boolean hapus(String no_beli){
        boolean adaKesalahan = false;
        Connection cn = null;

	try{
            Class.forName(Koneksi.driver);
        } catch (Exception ex){
            adaKesalahan = true;
            errorMessage="JDBC Driver tidak ditemukan atau rusak\n"+ex;
        }

	if (!adaKesalahan){
            try {
                cn = DriverManager.getConnection(Koneksi.database+"?user="+Koneksi.user+"&password="+Koneksi.password+"");
            } catch (Exception ex) {
                adaKesalahan = true;
                errorMessage="Koneksi ke database "+Koneksi.database+" gagal\n"+ex;
            }

            if (!adaKesalahan){
                int jumlahHapus;

                try {
                    String SQLStatemen = "delete from pembeli where no_beli='"+no_beli+"'";
                    Statement sta = cn.createStatement();
                    jumlahHapus = sta.executeUpdate(SQLStatemen);

                    if (jumlahHapus>0){
                        sta.close();
                        errorMessage="Data pembeli sudah dihapus";
                    } else {
                        sta.close();
                        errorMessage="Kode pembeli tidak ditemukan";
			adaKesalahan = true;
                    }
                } catch (Exception ex){
                    adaKesalahan = true;
                    errorMessage="Tidak dapat membuka tabel pembeli\n"+ex;
                }
            }
        }
        return !adaKesalahan;
    }
}
